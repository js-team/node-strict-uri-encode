# Installation
> `npm install --save @types/strict-uri-encode`

# Summary
This package contains type definitions for strict-uri-encode (https://github.com/kevva/strict-uri-encode#readme).

# Details
Files were exported from https://www.github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/strict-uri-encode

Additional Details
 * Last updated: Fri, 23 Mar 2018 22:51:43 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by Keiichiro Amemiya <https://github.com/hoishin>.
